import { Block } from "./block";
import { Transaction } from "./transaction";

export class FreeMarketChain {
	pendingTransactions: Transaction[];
	chain: Block[];
	miningReward: number;
	difficulty: number;
	
	constructor() {
		this.difficulty = 2;
		this.chain = [this.createGenesisBlock()];
		this.pendingTransactions = [];
		this.miningReward = 100;
	}

	createGenesisBlock() {
		return new Block(Date.now(), [], true, "Genesis Block Root");
	}

	getLatestBlock() {
		return this.chain[this.chain.length - 1];
	}

	minePendingTransactions(miningRewardAddress: string) {
		const rewardTransaction = new Transaction("MINER-ADDRESS", miningRewardAddress, this.miningReward, null);
		this.pendingTransactions.push(rewardTransaction);

		const block = new Block(Date.now(), this.pendingTransactions, false, this.getLatestBlock().hash);
		block.mineBlock(this.difficulty);

		this.chain.push(block);

		this.pendingTransactions = [];
	}

	createTransaction(transaction: Transaction) {
		this.pendingTransactions.push(transaction);
	}

	getBalanceOfAddress(address: string) {
		let balance = 0;

		for (const block of this.chain) {
			for (const trans of block.transactions) {
				if (trans.sender === address) {
					balance -= trans.amount;
				}

				if (trans.to === address) {
					balance += trans.amount;
				}
			}
		}

		return balance;
	}

	isChainValid() {
		for (let i = 1; i < this.chain.length; i++) {
			const currentBlock = this.chain[i];
			const previousBlock = this.chain[i - 1];

			if (currentBlock.hash !== currentBlock.calculateHash()) {
				return false;
			}

			if (currentBlock.previousHash !== previousBlock.hash) {
				return false;
			}
		}
		return true;
	}
}
