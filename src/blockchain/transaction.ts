// import 
const SHA256 = require('crypto-js/sha256');
const EC = require('elliptic').ec;

export class Transaction {
	sender: string;
	to: string;
	amount: number;
	signature: string | null;

	constructor(sender: string, to: string, amount: number, signature: string | null) {
		this.sender = sender;
		this.to = to;
		this.amount = amount;
		this.signature = signature;
	}

	calculateHash(): string {
		return SHA256(this.sender + this.to + this.amount).toString();
	}

	signTransaction(signingKey: typeof EC) {
		if (signingKey.getPublic('hex') !== this.sender) {
			throw new Error('Not allowed to sign transactions for another account');
		}

		const hashTx = this.calculateHash();
		const sig = signingKey.sign(hashTx, 'base64');
		this.signature = sig.toDER('hex');
	}

	isValid() {
		if (this.sender === null) return true;

		if (!this.sender || (this.signature && this.signature.length === 0)) {
			throw new Error('No signature in this transaction');
		}

		const publicKey = ec.keyFromPublic(this.sender, 'hex');
		return publicKey.verify(this.calculateHash(), this.signature);
	}
}