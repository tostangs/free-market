import { Transaction } from "./transaction";

const SHA256 = require('crypto-js/sha256');

export class Block {
	timestamp: number;
	transactions: Transaction[];
	previousHash: string;
	hash: string;
	nonce: number;
	data: any;

	constructor(timestamp: number, transactions: Transaction[], genesisBlock: boolean = false, previousHash = '') {
		if (!genesisBlock) {
			this.timestamp = timestamp;
		} else {
			this.timestamp = Date.now();
		}
		this.transactions = transactions;
		this.hash = this.calculateHash();
		this.previousHash = previousHash;
		this.nonce = 0;
	}

	calculateHash(): string {
		return SHA256(this.previousHash + this.timestamp + JSON.stringify(this.data) + this.nonce).toString();
	}

	// POW
	mineBlock(difficulty: number) {
		while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join('0')) {
			this.nonce++;
			this.hash = this.calculateHash();
		}

		console.log('Block mined: ' + this.hash);
	}
}